CREATE DATABASE IF NOT EXISTS cars_review;
USE cars_reviews;

CREATE TABLE IF NOT EXISTS cars(
id INT NOT NULL AUTO_INCREMENT,
marca VARCHAR(100) NOT NULL,
modelo VARCHAR(255) NOT NULL,
anho INT NOT NULL,
motor ENUM('Diésel', 'Gasolina', 'Híbrido', 'Eléctrico') DEFAULT 'Gasolina',
cv INT NULL,
createdAt DATETIME NOT NULL,
updatedAt DATETIME NULL,
deletedAt DATETIME NULL,
PRIMARY KEY (id)
);
