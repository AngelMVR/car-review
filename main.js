"use strict";
require("dotenv").config();
const express = require("express");
const app = express();
app.use(express.json());
// app.use(express.urlencoded)->si te envían un formulario
const port = process.env.SERVER_PORT || 3000;
const carsRouter = require("./app/routes/car-routes");

// /api/v1/cars
// /api/v1/cars/id
app.use("/api/v1/cars/", carsRouter);

app.listen(port, () => console.log(`Escuchando puerto ${port}`));
