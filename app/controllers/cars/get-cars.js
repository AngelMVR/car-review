"use strict";

const { findAllCars } = require("../../repositories/cars-respository");

// const { findAllCars } =
//     const carsRepository=

async function getCars(req, res) {
  try {
    //VALIDAR VARIABLES
    //console.log("getCars");
    // const cars=await carsRepository.findAllCars()
    const cars = await findAllCars();
    // res.statusCode = 200;
    res.status(200);
    res.send(cars);
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = { getCars };
