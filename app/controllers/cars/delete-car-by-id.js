"use strict";

const Joi = require("joi");
const {
  removeCarById,
  findCarById,
} = require("../../repositories/cars-respository");
const schema = Joi.number().integer().positive().required();
async function deleteCarById(req, res) {
  try {
    const { idCar } = req.params;
    await schema.validateAsync(idCar);
    const car = await findCarById(idCar);
    if (!car) {
      throw new Error("Coche no existe");
    }
    await removeCarById(idCar);
    res.status(204);
    res.end();
    //res.status(204).send(`${idCar} borrado correctamente`)
  } catch (err) {
    console.log(err.message);
  }
}

module.exports = { deleteCarById };
