"use strict";

// const { getCarById } = require("../controllers/cars/get-car-by-id");
// const { delete } = require("../routes/car-routes");

const cars = [
  {
    id: 1,
    marca: "Seat",
    modelo: "Ibiza",
    anho: 2019,
    motor: "v6",
    cv: 125,
    combustion: "gasolina",
  },
  {
    id: 2,
    marca: "Opel",
    modelo: "Corsario",
    anho: 2014,
    motor: "v4",
    cv: 80,
    combustion: "diesel",
  },
  {
    id: 3,
    marca: "Audi",
    modelo: "A3",
    anho: 2017,
    motor: "v8",
    cv: 150,
    combustion: "gasolina",
  },
];

async function findAllCars() {
  //await SELECT * FROM cars
  return cars;
}

async function findCarById(id) {
  //LLamada real a la BB.DD.
  return cars.find((car) => car.id === +id);
}

async function addCar(car) {
  //llamada a la base de datos
  // const id = Math.max(...cars.map((car) => car.id)) + 1;
  const id = cars.reduce((acc, car) => (car.id > acc ? car.id : acc), 0) + 1;
  console.log("nuevoID", id);
  const newCar = {
    id,
    // id: idUser,
    ...car,
  };
  cars.push(newCar);
  return true;
  //const newCar={
  // id,
  //   marca: car.marca,
  //     modelo: car.modelo,
  // }
}

async function removeCarById(idCar) {
  console.log("borrado...");
  const car = await findCarById(idCar);
  const index = cars.indexOf(car);
  return cars.splice(index, 1);
  // const cars.find((car) => car.id === +id);
  // cars.pop(car.getCarById())
  // cars.delete(cars[id - 1]);
}

module.exports = {
  addCar,
  removeCarById,
  findAllCars,
  findCarById,
};
