"use strict";

const express = require("express");
const router = express.Router();
//const { getCars } = require("./app/controllers/cars/get-cars");
const { getCars } = require("../controllers/cars/get-cars");

// router.route("/").get((req, res) => {
//   getCars(req, res);
// });
const { createCar } = require("../controllers/cars/create-car");

const { getCarById } = require("../controllers/cars/get-car-by-id");

const { deleteCarById } = require("../controllers/cars/delete-car-by-id");
//router.route('/')
// .get(getCars)
// .post(createCar);
//equivalente:
router.route("/").get(getCars);

router.route("/").post(createCar);

router.route("/:idCar").get(getCarById);
//cars.getCars si no ponemos el destructuring de arriba {getCars}
router.route("/:idCar").get(getCarById).delete(deleteCarById);

module.exports = router;
